// Budget Controller
var budgetController = (function(){
    
    var Expense = function(id,description,value){
        this.id = id;
        this.description = description;
        this.value = value;
        this.percentage = -1;
    };
    
    var Income = function(id,description,value){
        this.id = id;
        this.description = description;
        this.value = value;
    };
    
    Expense.prototype.calcPercentage = function(totalIncome){
        if(totalIncome>0){
            this.percentage = Math.round((this.value/totalIncome)*100);
        }
    };
    Expense.prototype.getPercentage = function(){
        return this.percentage;
    }

    var calculateTotal = function(type){
        var sum = 0;
        data.allItems[type].forEach(function(cur){
            sum+=cur.value;
        });
        data.totals[type] = sum;
    };
    
    var data  = {
        allItems:{
            exp: [],
            inc: []
        },
        totals: {
            exp: 0,
            inc: 0
        },
        budget: 0,
        percentage: -1
    }
    
    return {
        addItem: function(type, des,val){
            var newItem,ID;
            // Create new ID
            if (data.allItems[type].length > 0){
                ID = data.allItems[type][data.allItems[type].length - 1].id + 1;    
            }else{
                ID = 0;
            }
            // Create new item based on 'inc' or 'exp' type
            if (type === 'exp'){
                newItem = new Expense(ID,des,val);    
            }else if (type === 'inc') {
                newItem = new Income(ID,des,val);
            }
            // Push it into out data structure
            data.allItems[type].push(newItem);
            
            // Return the new element
            return newItem;
            
        },
        deleteItem: function(ID,type){
            var ids,index;
            ids = data.allItems[type].map(function(cur){
                return cur.id;
            });
            index = ids.indexOf(ID);
            if(index != -1 ){
                data.allItems[type].splice(index,1);
            }
        },
        
        calculateBudget: function(){
            var income,expense;
            //Sum of all incomes and expenses
            calculateTotal('exp');
            calculateTotal('inc');
            //Calculate budget
            data.budget = data.totals.inc - data.totals.exp;
            //Percentage of total expense
            data.percentage = (data.totals.inc > 0 ? Math.round((data.totals.exp/data.totals.inc) * 100) : '--');
            
        },
        calculatePercentages: function(){
            data.allItems.exp.forEach(function(cur){
                cur.calcPercentage(data.totals.inc);    
            });
        },
        
        getPercentages: function(){
            var allPercentages = data.allItems.exp.map(function(cur){
                return cur.getPercentage(); 
            });
            return allPercentages;
        },
        getBudget: function(){
            return {
                budget: data.budget,
                totalInc: data.totals.inc,
                totalExp: data.totals.exp,
                percentage: data.percentage
            }
        },
        
        test: function(){
            return data.percentage;
            
        }
    };
})();

// --------------------------------------------------------------------
// UI controller
var UIController = (function(){
    var DOMstrings = {
        inputType: '.add__type',
        inputDescription: '.add__description',
        inputValue: '.add__value',
        inputButton: '.add__btn',
        incomeContainer: '.income__list',
        expensesContainer: '.expenses__list',
        budgetLabel: '.budget__value',
        incomeLabel: '.budget__income--value',
        expensesLabel: '.budget__expenses--value',
        percentageLabel: '.budget__expenses--percentage',
        container: '.container',
        expensesPerLabel: '.item__percentage',
        expensesClearfix: '.right clearfix',
        dateLabel: '.budget__title--month'
    };
    var formatNumber =  function(num,type){
        var numSplit,int,dec,type,otherInt;
        num = Math.abs(num);
        num = num.toFixed(2);
        numSplit = num.split('.');
        int = numSplit[0];
        if(int.length > 3){
            int = int.substr(0,int.length - 3) + ','+int.substr(int.length-3,3);
            /*if(int.length>5){
                otherInt = int.substring(0,int.length - 5);
                if(otherInt.length%2 === 0){
                    //When string is even
                    for(var i=0;i < otherInt.length;i++){
                        var x;
                        x = otherInt.split("",2);
                        console.log(x);
                    }*/
                /*}else{
                    //When string is odd
                    
                }*/
            //}
        }
        dec = numSplit[1];
        return (type === 'exp'? '-':'+')+' '+int+'.'+dec;
    };
    var nodeListForEach = function(list, callback){
        for (var i = 0;i<list.length;i++){
            callback(list[i],i);
        }
    };
    return {
        getInput: function() {
            return {
                type : document.querySelector(DOMstrings.inputType).value, // Will be inc/exp
                description : document.querySelector(DOMstrings.inputDescription).value,
                value : parseFloat(document.querySelector(DOMstrings.inputValue).value)
            }
        },
        
        addListItem: function(obj, type) {
            // Create HTML string with placeholer text
            var html, newHtml, element;
            if (type == 'inc'){
                element = DOMstrings.incomeContainer;
                
                html = '<div class="item clearfix" id="inc-%id%"><div class="item__description">%description%</div><div class="right clearfix"><div class="item__value">%value%</div><div class="item__delete"><button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div></div>';
            }else if (type == 'exp'){
                element = DOMstrings.expensesContainer;
                
                html = '<div class="item clearfix" id="exp-%id%"><div class="item__description">%description%</div><div class="right clearfix"><div class="item__value">%value%</div><div class="item__percentage">21%</div><div class="item__delete"><button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div></div>';   
            }
            // Replace the placeholder text with some actual data
            newHtml = html.replace('%id%', obj.id);
            newHtml = newHtml.replace('%description%',obj.description);
            newHtml = newHtml.replace('%value%',formatNumber(obj.value,type));
            
            // Insert the HTML into the DOM
            document.querySelector(element).insertAdjacentHTML('beforeend',newHtml);
            
        },
        
        deleteListItem: function(selectorID){
            var element;
            element = document.getElementById(selectorID);
            element.parentNode.removeChild(element);
        },
        
        clearFields: function(){
            var fields,fieldsArray;
            
            // querySelectorAll will return a list. Hence we convert it to array.
            fields = document.querySelectorAll(DOMstrings.inputDescription+','+DOMstrings.inputValue);    
            fieldsArray = Array.prototype.slice.call(fields);
            fieldsArray.forEach(function(cur,i,arr){
                cur.value = "";
            });
            fieldsArray[0].focus();
        },
        
        showBudget: function(obj){
            var type;
            obj.budget >= 0 ? type = 'inc':type = 'exp';
            document.querySelector(DOMstrings.budgetLabel).textContent = formatNumber(obj.budget,type);
            document.querySelector(DOMstrings.incomeLabel).textContent = formatNumber(obj.totalInc,'inc');
            document.querySelector(DOMstrings.expensesLabel).textContent = formatNumber(obj.totalExp,'exp');
            document.querySelector(DOMstrings.percentageLabel).textContent = obj.percentage;
            
        },
        displayPercentages: function(perArr){
            var fields = document.querySelectorAll(DOMstrings.expensesPerLabel);
            
            
            nodeListForEach(fields,function(current, index){
                if(perArr[index] >0){
                    current.textContent = perArr[index] + '%';    
                }else{
                    current.textContent = "---";
                }
            });
        },
        
        changedType: function(){
            var fields = document.querySelectorAll(
                DOMstrings.inputType + ',' +
                DOMstrings.inputDescription+','+
                DOMstrings.inputValue);
            
            nodeListForEach(fields,function(cur){
                cur.classList.toggle('red-focus');
            })
            document.querySelector(DOMstrings.inputButton).classList.toggle('red');
        },
        
        displayMonth: function(){
            var now,year,month,months;
            now = new Date();
            year = now.getFullYear();
            months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
            month = now.getMonth();
            document.querySelector(DOMstrings.dateLabel).textContent = months[month]+", " +year+" ";
        },
        getDOMstrings: function(){
            return DOMstrings;
        }
    };
})();


//--------------------------------------------------------------------
//Global Controller
var controller = (function(budgetCtrl,UICtrl){
    
    var setupEventListeners = function(){
        var DOM = UICtrl.getDOMstrings();
        document.querySelector(DOM.inputButton).addEventListener('click',ctrlAddItem);
        // Enter event listener
        document.addEventListener('keypress',function(event) {
            if(event.keyCode === 13|| event.which === 13){
                ctrlAddItem();
            }
        });
        
        document.querySelector(DOM.expensesContainer).addEventListener('click', ctrlDeleteItem);
        
        document.querySelector(DOM.inputType).addEventListener('change',UICtrl.changedType);
        
    };   
    
    var updateBudget = function(){
        // Calculate the budget
        budgetCtrl.calculateBudget();        
        // Return the budget
        var budget = budgetCtrl.getBudget();        
        // Display the budget on the UI
        UICtrl.showBudget(budget);
        
    };
    
    
    
    var ctrlAddItem = function(){
        var input, newItem;
        
        // Get field input data
        input = UICtrl.getInput();
        
        //Checking if there is an input
        if(input.description!== "" && input.value !== NaN && input.value > 0){
            
            // Add the item to the budget controller(ds)
            newItem = budgetCtrl.addItem(input.type,input.description,input.value);

            // Add the item to UI
            UICtrl.addListItem(newItem,input.type);

            //Empty the fields of input
            UICtrl.clearFields();

            // Calculate and update budget
            
            updateBudget();
            
            //Update Percentages
            updatePercentages();
            
        }
        
    };
    
    var ctrlDeleteItem = function(event){
        var itemID,splitID,type,ID;
        // Add event listeners for each block -- Done in the UICtrl
        itemID = event.target.parentNode.parentNode.parentNode.parentNode.id;
        if(itemID){
            // inc-1 or exp-1
            splitID = itemID.split('-');
            type = splitID[0];
            ID = parseInt(splitID[1]);
        }
        // Delete the item in the database
        budgetCtrl.deleteItem(ID,type);
        
        //Delete from UI
        UICtrl.deleteListItem(itemID);
        
        // Re-calculate the budget
        updateBudget(); 
        
        // Update percentages
        updatePercentages();
    };
    
    var updatePercentages = function(){
        //Calculate Percentage
        budgetCtrl.calculatePercentages();
        
        //Read Percentages from the budget controller    
        var allPer = budgetCtrl.getPercentages();
        
        //Update in UI
        UICtrl.displayPercentages(allPer);
        
    };
    
    return{
        init: function(){
            setupEventListeners();
            UICtrl.showBudget({budget:0 ,totalInc: 0,totalExp: 0,percentage: '---'});
            UICtrl.displayMonth();
        }
    }
})(budgetController,UIController);
// --------------------------------------------------------------------
controller.init();